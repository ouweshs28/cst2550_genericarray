public class Tester {
    public static void main(String[] args) {
        GenericArray<Integer> a1 = new GenericArray<>(5);
        for(int i = 0;i<5; ++i)
            a1.set(i,i);
        System.out.println(a1);
        GenericArray<Double> a2 = new GenericArray<>(5);
        for(int i = 0;i<5; ++i)
            a2.set(i,i /2.0);
        System.out.println(a2);
    }
}
