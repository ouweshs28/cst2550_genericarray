import java.util.Arrays;

public class GenericArray<T> {

    private T[] data;

    public GenericArray(int size) {
        data = (T[]) new Object[size];
    }

    public void set(int i, T val) {
        data[i] = val;
    }

    public T get(int i) {
        return data[i];
    }

    @Override
    public String toString() {
        return "GenericArray{" +
                "data=" + Arrays.toString(data) +
                '}';
    }
}
